import numpy as np
import pandas as pd
import typing as tp
import math
import xgboost
import pickle
# import joblib


def save_obj(obj, name):
    with open('data/' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    dct = {}
    with pd.read_csv(name, compression='gzip', chunksize=10**3) as reader:
        for chunk in reader:
            dct.update(dict(zip(chunk['char'], chunk['code'])))

    return dct


def read_file(file):
    with open(file, 'r') as f:
        for line in f:
            if len(line) > 4:
                yield line.rstrip()


def entropy(string):
    """Calculates the Shannon entropy of a string"""
    # get probability of chars in string
    prob = [float(string.count(c)) / len(string) for c in dict.fromkeys(list(string))]
    # calculate the entropy
    entropy = - sum([p * math.log(p) / math.log(2.0) for p in prob])
    return entropy

                
def entropy_ideal(length):
    """Calculates the ideal Shannon entropy of a string with given length"""
    prob = 1.0 / length
    return -1.0 * length * prob * math.log(prob) / math.log(2.0)


def downcast_dtypes(df):
    """
        Changes column types in the dataframe:

                `float64` type to `float32`
                `int64`   type to `int32`
    """

    # Select columns to downcast
    float_cols = [c for c in df if df[c].dtype == "float64"]
    int_cols = [c for c in df if df[c].dtype == "int64"]

    # Downcast
    df[float_cols] = df[float_cols].astype(np.float32)
    df[int_cols] = df[int_cols].astype(np.int32)

    return df


def rmsle(predt: np.ndarray, dtrain: xgboost.DMatrix) -> tp.Tuple[str, float]:
    """ Root mean squared log error metric."""
    y = dtrain.get_label()
    predt[predt < -1] = -1 + 1e-6
    elements = np.power(np.log1p(y) - np.log1p(predt), 2)
    return 'PyRMSLE', float(np.sqrt(np.sum(elements) / len(y)))


def gradient(predt: np.ndarray, dtrain: xgboost.DMatrix) -> np.ndarray:
    """Compute the gradient squared log error."""
    y = dtrain.get_label()
    return (np.log1p(predt) - np.log1p(y)) / (predt + 1)


def hessian(predt: np.ndarray, dtrain: xgboost.DMatrix) -> np.ndarray:
    """Compute the hessian for squared log error."""
    y = dtrain.get_label()
    return ((-np.log1p(predt) + np.log1p(y) + 1) /
            np.power(predt + 1, 2))


def squared_log(predt: np.ndarray,
                dtrain: xgboost.DMatrix) -> tp.Tuple[np.ndarray, np.ndarray]:
    """Squared Log Error objective. A simplified version for RMSLE used as
    objective function.
    """
    predt[predt < -1] = -1 + 1e-6
    grad = gradient(predt, dtrain)
    hess = hessian(predt, dtrain)
    return grad, hess