import pandas as pd
import numpy as np
import xgboost
import joblib

from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from itertools import combinations
from password_strength import PasswordStats
from names_dataset import NameDatasetV1
from model.functions import read_file, save_obj, load_obj, rmsle, squared_log, \
    entropy, downcast_dtypes


class SimpleModel:

    def __init__(self, root):
        # R15 error https://stackoverflow.com/questions/49804356/heroku-memory-quota-vastly-exceeded-in-django-project-using-sklearn
        # self.train = pd.read_csv(f'{root}/data/train.csv')  # .head(100000) #
        self.train = pd.DataFrame({'Password': ['1234', 'sasdfgh', 'sddksfkj'],
                                   'Times': [10, 20, 30]})
        self.target = None
        self.model = xgboost.Booster({'nthread': 4})
        self.model.load_model('data/0001.model')
        self.names_set = NameDatasetV1()
        self.words_set = set(read_file(f'{root}/data/google_10k_words/google-10000-english.txt'))
        self.last_chars = ['last_char4', 'last_char3', 'last_char2', 'last_char']
        self.first_chars = ['first_char4', 'first_char3', 'first_char2', 'first_char']

        self.last_chars_encoder = load_obj(f'{root}/data/last_chars.csv.gz')
        self.first_chars_encoder = load_obj(f'{root}/data/first_chars.csv.gz')
        self.results = {}

    def dummy_method(self):
        return 99

    def load_xgb_model(self):
        self.model.load_model('data/0001.model')

    def save_xgb_model(self, path):
        self.model.save_model(path + '.model')

    def prepare_dataframe(self):
        self.train.dropna(inplace=True)
        self.target = self.train.Times.values

    def featuring(self, df):
        df['len'] = df['Password'].str.len()
        df['digit'] = df['Password'].str.contains('\\d', regex=True).astype(int)
        df['lower'] = df['Password'].str.contains('[a-z]', regex=True).astype(int)
        df['upper'] = df['Password'].str.contains('[A-Z]', regex=True).astype(int)
        df['any_letter'] = df['Password'].str.contains('[A-Za-z]', regex=True).astype(int)
        df['is_decimal'] = df['Password'].str.isdecimal().astype(int)
        df['is_alnum'] = df['Password'].str.isalnum().astype(int)
        df['mt_8char'] = df['Password'].apply(lambda x: len(x) >= 8).astype(int)
        df['special_char'] = df['Password'].str.contains("[^a-zA-Z0-9]+", regex=True).astype(int)
        df['n_unique_chars'] = df['Password'].apply(lambda x: len(set(x)))
        df['n_repeated_chars'] = df['len'] - df['n_unique_chars']
        df['n_unique_chars_share'] = round((df['len'] - df['n_repeated_chars']) / df['len'], 4)
        df['char_sum'] = df['digit'] + df['lower'] + df['upper'] + df['any_letter'] + \
                         df['is_decimal'] + df['special_char'] + df['is_alnum'] + df[
                             'mt_8char']
        df['entropy'] = df['Password'].apply(lambda x: entropy(x))
        df['entropy_bits'] = df['Password'].apply(lambda x: PasswordStats(x).entropy_bits)
        df['strength'] = df['Password'].apply(lambda x: PasswordStats(x).strength())
        df['weakness_factor'] = df['Password'].apply(lambda x: PasswordStats(x).weakness_factor)
        df['password_strength'] = (1 - df['weakness_factor']) * df['strength']
        df['first_char'] = df['Password'].apply(lambda x: x[:1])
        df['first_char2'] = df['Password'].apply(lambda x: x[:2])
        df['first_char3'] = df['Password'].apply(lambda x: x[:3])
        df['first_char4'] = df['Password'].apply(lambda x: x[:4])
        df['last_char'] = df['Password'].apply(lambda x: x[-1:])
        df['last_char2'] = df['Password'].apply(lambda x: x[-2:])
        df['last_char3'] = df['Password'].apply(lambda x: x[-3:])
        df['last_char4'] = df['Password'].apply(lambda x: x[-4:])
        df['first_char_ord'] = df['first_char'].apply(lambda x: ord(x) if len(x) == 1 else 0)
        df['last_char_ord'] = df['last_char'].apply(lambda x: ord(x) if len(x) == 1 else 0)
        df['combinations_4char'] = df['Password'].apply(lambda x: len(list(combinations(x, 4))))
        df['Password_letters'] = df['Password'].apply(lambda x: ''.join(filter(str.isalpha, x.lower())))
        df['is_popular_word'] = df['Password_letters'].apply(lambda x: int(x in self.words_set) if x else 0)
        df['is_name'] = df['Password_letters'].apply(
            lambda x: int(self.names_set.search_first_name(x)) if x else 0)
        # df['is_name'] = 0

        return df

    def _preprocessing_train(self):
        le = preprocessing.LabelEncoder()
        train_df_copy = self.train.copy()
        self.last_chars_encoder = {}
        self.first_chars_encoder = {}

        for col in self.last_chars:
            train_df_copy[col] = le.fit_transform(self.train[col])
            self.last_chars_encoder.update(dict(zip(le.classes_, le.transform(le.classes_))))

        for col in self.first_chars:
            train_df_copy[col] = le.fit_transform(self.train[col])
            self.first_chars_encoder.update(dict(zip(le.classes_, le.transform(le.classes_))))

        #         save_obj(last_chars_encoder, 'last_chars_encoder')
        #         save_obj(first_chars_encoder, 'first_chars_encoder')
        return train_df_copy

    def _preprocessing_test(self, password_df):
        password_df_copy = password_df.copy()

        for col in self.last_chars:
            password_df_copy[col] = password_df[col].apply(
                lambda x: self.last_chars_encoder.get(x, len(self.last_chars_encoder)))
        for col in self.first_chars:
            password_df_copy[col] = password_df[col].apply(
                lambda x: self.first_chars_encoder.get(x, len(self.first_chars_encoder)))
        return password_df_copy

    def train_df(self):
        self.prepare_dataframe()
        self.featuring(self.train)
        X = self._preprocessing_train()
        X.drop(['Password', 'Password_letters', 'Times'], inplace=True, axis=1)
        X = downcast_dtypes(X)
        X_train, X_test, y_train, y_test = train_test_split(X, self.target,
                                                            test_size=0.25, random_state=42)
        dtrain = xgboost.DMatrix(X_train, y_train, missing=0.0, nthread=-1)
        dtest = xgboost.DMatrix(X_test, y_test, missing=0.0, nthread=-1)

        self.model = xgboost.train({'tree_method': 'hist', 'seed': 1994,
                                    'disable_default_eval_metric': 1},
                                   dtrain=dtrain,
                                   num_boost_round=500,
                                   obj=squared_log,
                                   early_stopping_rounds=30,
                                   feval=rmsle,
                                   verbose_eval=True,
                                   evals=[(dtrain, 'dtrain'), (dtest, 'dtest')],
                                   evals_result=self.results)

    def predict(self, password):
        password_df = pd.DataFrame({'Password': [password]})
        password_f = self.featuring(password_df)
        password_df = self._preprocessing_test(password_f)
        y = password_df.drop(['Password', 'Password_letters'], axis=1)
        y = xgboost.DMatrix(y, missing=0.0, nthread=-1)
        return self.model.predict(y)[0]

