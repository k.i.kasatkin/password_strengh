from flask import Flask, request, render_template
from model.baseline_model import SimpleModel
import os
app = Flask(__name__)

root = os.path.dirname(os.path.abspath(__file__))
model = SimpleModel(root)


@app.route("/", methods=["POST", "GET"])
def index():
    if request.method == 'POST':
        password = request.form['password']

        if not password:
            return render_template('index.html', password='', prediction=0)
        else:
            pred = model.predict(password)
            return render_template('index.html', password=password, prediction=pred)

    return render_template('index.html')


if __name__ == "__main__":
    app.run(host="0.0.0.0", threaded=False, debug=True)

